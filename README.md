<p align="center">
<a href="https://travis-ci.com/matthieutirelli/damoviequizz_server"><img src="https://travis-ci.com/matthieutirelli/damoviequizz_server.svg?token=6Y2mhZrXDTceeW2tRf9p&branch=master" alt="Build Status"></a>
</p>

## About Da Movie Quizz

Da Movie Quizz is a cinema culture test

## How to run the quizz

You need to have Docker on your machine

You can build the docker file by a simple :

```docker-compose up```

The server will then be exposed on port 8000