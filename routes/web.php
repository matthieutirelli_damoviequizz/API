<?php

header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/game')->group(function() {
    Route::get('/start', 'GameController@start');
    Route::get('/{game_uuid}/info/', 'GameController@info');
    Route::prefix('/{game_uuid}/challenge')->group(function() {
        Route::get('/', 'GameController@challenge');
        Route::post('/{challenge_uuid}/answer', 'GameController@answer');
    });
});
