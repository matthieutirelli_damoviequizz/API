FROM php:7.2-fpm
MAINTAINER Matthieu TIRELLI <matthieu.tirelli@gmail.com>

RUN apt-get update && apt-get install -y \
	build-essential \
	locales \
	zip \
	vim \
	git \
	curl \
	unzip

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN mkdir /code
WORKDIR /code

# Setup user
RUN groupadd -r developers && useradd -r -g developers dockerdev

# Setup SSH KEY for tmate
RUN mkdir /home/dockerdev
RUN mkdir /home/dockerdev/.ssh
RUN ssh-keygen -b 2048 -t rsa -f /home/dockerdev/.ssh/id_rsa -q -N ""
RUN chown dockerdev:developers -R /home/dockerdev

ADD . /code/
RUN composer install

EXPOSE 8000
USER dockerdev
