<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use App\RedisModel\Game;

/**
 * Class GameController
 * @package App\Http\Controllers
 */
class GameController extends Controller
{
    /**
     * @return array|null
     * @throws \Exception
     */
    public function start() {
        $game = new Game(Uuid::uuid4());
        return $game->start();
    }

    /**
     * @param Request $request
     * @return null
     * @throws \Exception
     */
    public function info(Request $request) {
        $game = new Game($request->game_uuid);
        return $game->getData();
    }

    /**
     * @param Request $request
     * @return array|null
     * @throws \Exception
     */
    public function challenge(Request $request) {
        $game = new Game($request->game_uuid);
        return $game->challenge();
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function answer(Request $request) {
        $game = new Game($request->game_uuid);
        return $game->answer($request->challenge_uuid, $request->answer);
    }
}
