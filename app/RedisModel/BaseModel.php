<?php

namespace App\RedisModel;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

/**
 * Class BaseModel
 * @package App\RedisModel
 */
class BaseModel{

    /**
     * BaseModel constructor.
     * @param string $uuid
     * @throws \Exception
     */
    public function __construct(string $uuid)
    {
        if (!$uuid) {
            throw new \Exception('You must provide an uuid to load a model');
        }

        $this->uuid = $uuid;
        $this->model_type = null;
        $this->data = null;
    }

    /**
     * @return string
     */
    protected function getRedisKey() {
        return $this->model_type.'_'.$this->uuid;
    }

    /**
     * @param $uuid
     * @throws \Exception
     */
    protected function load() {

        /* Data are loaded for this model we dont fetch it from redis again */
        if($this->data) {
            return;
        }

        $model_json = Cache::get(
            $this->getRedisKey()
        );

        if ($model_json === null) {
            abort(403);
        }

        $this->data = json_decode((string)$model_json, (bool)true);
    }

    /**
     * @param $key
     * @param $value
     */
    public function set(string $key, $value) {
        $this->data[$key] = $value;
        $this->save();
    }

    /**
     * @param $key
     * @return string with data or null
     */
    public function get(string $key) {
        if (!$this->data) {
            try {
                $this->load();
            } catch (\Exception $e) {
                return null;
            }
        }
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }

    public function getData() {
        if (!$this->data) {
            try {
                $this->load();
            } catch (\Exception $e) {
                abort(500, 'An error occured fetching the model');
            }
        }



        return $this->data;
    }

    /**
     *
     */
    public function save() {
        Cache::set(
            $this->getRedisKey(),
            json_encode($this->data)
        );
    }

}