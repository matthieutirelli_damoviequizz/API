<?php

namespace App\RedisModel;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;

/**
 * Class Game
 * @package App\RedisModel
 */
class Game extends BaseModel {

    /**
     * Game constructor.
     * @param string $uuid
     * @throws \Exception
     */
    public function __construct(string $uuid)
    {
        parent::__construct($uuid);
        $this->model_type = "game";
        $this->game_duration = 60;
        $this->challenge = null;
        $this->client = new Client();
    }

    /**
     * Load game data from redis
     * @throws \Exception
     */
    protected function load() {
        parent::load();
        if ($challenge_uuid = $this->get('challenge_key')) {
            $this->challenge = new Challenge($challenge_uuid, $this->client);
        }
    }

    /**
     * Start a new game session and set data into redis
     * @return array|null
     */
    public function start() {

        $this->data = [
            'uuid'     => $this->uuid,
            'start'    => Carbon::now()->toISOString(),
            'end'      => Carbon::now()->addSecond($this->game_duration)->toISOString(),
            'duration' => $this->game_duration,
            'active'   => true,
            'points'   => 0,
        ];

        $this->save();

        $private_fields = [
            'start',
            'end'
        ];

        return array_filter(
            $this->data,
            function ($key) use ($private_fields) {
                return !in_array($key, $private_fields);
            },
            ARRAY_FILTER_USE_KEY
        );
        }

    /**
     * Retrieve or get a new challenge
     * @return array|null
     * @throws \Exception
     */
    public function challenge() {

        $this->load();

        if ($challenge = $this->challenge) {
            return $challenge->getData();
        }

        $challenge = new Challenge(Uuid::uuid4(), $this->client);
        $points = $this->get('points') ?: 1;
        $challenge->start($points);
        $challenge->setGameUuid($this->uuid);

        $this->challenge = $challenge;
        $this->set('challenge_key', $challenge->uuid);
        return $challenge->getData();
    }

    /**
     * Reset Challenge key to get a new challenge
     */
    public function resetChallenge() {
        $this->set('challenge_key', null);
        $this->challenge = null;
    }

    /**
     * Increments game score
     */
    private function incrementPoints() {
        $current = $this->get('points');
        $this->set('points', $current + 1);
    }

    /**
     * Stop the current game, ex : failure reason
     */
    private function stopGame() {
        $this->set('active', false);
    }

    /**
     * Check if game is still playable
     * @return bool
     */
    private function isPlayable() {

        $start = $this->get('start');
        $end = $this->get('end');

        if (Carbon::parse($start) > Carbon::now()) {
            return false;
        }

        if (Carbon::parse($end) < Carbon::now()) {
            return false;
        }

        if ($this->get('active') === false) {
            return false;
        }

        return true;
    }

    /**
     * Send an answer to the game challenge, and do the appropriate actions
     * @param string $challengeUuid
     * @param bool $answer
     * @return array
     * @throws \Exception
     */
    public function answer(string $challengeUuid, bool $answer) {
        $this->load();

        if ($this->isPlayable()) {

            if (!$this->challenge) {
                abort(404);
            }

            if ($challengeUuid !== $this->challenge->uuid) {
                abort(500, 'Invalid challenge uuid provided');
            }

            if ($this->challenge->answerEqual($answer)) {
                $this->incrementPoints();
                $this->resetChallenge();

                return [
                    'message' => 'MSG_GAME_SUCCESS',
                    'points' => $this->get('points')
                ];

            }

            $this->stopGame();

            return [
                'message' => 'MSG_GAME_FAILURE',
                'points' => $this->get('points')
            ];

        }

        return [
            'message' => 'MSG_GAME_TIMEOUT',
            'points' => $this->get('points')
        ];
    }
}