<?php

namespace App\RedisModel;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class Challenge
 * @package App\RedisModel
 */
class Challenge extends BaseModel
{

    /**
     * Challenge constructor.
     * @param string $uuid
     * @throws \Exception
     */
    public function __construct(string $uuid, $client)
    {
        parent::__construct($uuid);
        $this->model_type = "challenge";
        $this->api_key = "a16ac24c815a6d7c53fd0077b1bd82a5";
        $this->client = $client;
    }

    /**
     * Set the game uuid into the challenge to avoid any challenge misusage
     * @param string $uuid
     */
    public function setGameUuid(string $uuid) {
        $this->set('game_uuid', $uuid);
    }

    /**
     * @param $movieId
     * @return mixed
     * @throws GuzzleException
     */
    public function getMovieDetails($movieId) {
        $params = http_build_query([
            'api_key' => $this->api_key
        ]);

        $res = $this->client->get('https://api.themoviedb.org/3/movie/'.$movieId.'/credits?'.$params);
        if ($res->getStatusCode() !== 200) {
            throw new GuzzleException('Couldnt fetch data source');
        }

        return json_decode($res->getBody());
    }

    private function getRandomCastMember(array $cast, int $maxPosition = 5) {
        $slice = array_slice($cast, 0, $maxPosition);
        $random = array_rand($slice);
        return $cast[$random];
    }

    /**
     * @param $movieId
     * @return mixed
     * @throws GuzzleException
     */
    public function getCastMember($movieId) {
        $movie = $this->getMovieDetails($movieId);
        return $this->getRandomCastMember($movie->cast);
    }

    public function getCastAndCompare($movieId, $compareId) {
        $movie = $this->getMovieDetails($movieId);
        $new_cast_member = $this->getRandomCastMember($movie->cast);

        $is_valid = false;

        foreach($movie->cast as $cast_member) {
            if ($cast_member->id === $compareId) {
             $is_valid = true;
             break;
            }
        }
        return [$new_cast_member, $is_valid];
    }

    /**
     * @param $page
     * @return mixed
     * @throws GuzzleException
     */
    public function getMovies($page) {
        $params = http_build_query([
            'api_key' => $this->api_key,
            'language' => 'fr-FR',
            'page' => $page,
            'sort_by' => 'popularity.desc'
        ]);

        $res = $this->client->get('https://api.themoviedb.org/3/discover/movie?'.$params);

        if($res->getStatusCode() !== 200) {
            throw new GuzzleException('Couldnt fetch data source');
        }

        return json_decode($res->getBody());
    }


    /**
     * @param $page
     * @return mixed
     * @throws GuzzleException
     */
    public function getMovie($page) {
        $movies = $this->getMovies($page);
        $random = array_rand($movies->results);

        return $movies->results[$random];
    }

    /**
     * Start a new challenge
     * @return array|null
     */
    public function start($page = 1) {

        $movie = $this->getMovie($page);
        $cast_member = $this->getCastMember($movie->id);

        $is_valid = $movie->id % 2 === 0;


        if (!$is_valid) {
            $new_movie = $this->getMovie($page + 1);
            list($cast_member, $is_valid) = $this->getCastAndCompare($new_movie->id, $cast_member->id);
        }

        $prefix_pictures = 'https://image.tmdb.org/t/p/w500';

        $this->data = [
            'uuid'          => $this->uuid,
            'title'         => $movie->original_title,
            'picture'       => $prefix_pictures.$movie->poster_path,
            'actor'         => $cast_member->name,
            'actor_picture' => $prefix_pictures.$cast_member->profile_path,
            'is_valid'      => $is_valid,
        ];

        $this->save();

        return $this->data;
    }

    /**
     * Override the getData from the base model to filter with some private fields only known by this class
     * @return array|null
     * @throws \Exception
     */
    public function getData() {
        $this->load();

        $private_fields = [
            'is_valid'
        ];

        return array_filter(
            $this->data,
            function ($key) use ($private_fields) {
                return !in_array($key, $private_fields);
            },
            ARRAY_FILTER_USE_KEY
        );
    }

    /**
     * Compare the answer to the provided answer
     * @param bool $answer
     * @return bool
     * @throws \Exception
     */
    public function answerEqual(bool $answer) {
        $this->load();
        return $this->get('is_valid') === $answer;
    }
}