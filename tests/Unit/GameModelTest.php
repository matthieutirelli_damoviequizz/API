<?php

namespace Tests\Unit;

use App\RedisModel\Game;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Cache;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;


class GameModelTest extends TestCase
{
    public function getTestGameModel() {
        return new Game('e82c5370-840e-4906-8bdf-af3e370899d2');
    }


    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    public function test_start()
    {

        $gameData = [
            'uuid' => 'e82c5370-840e-4906-8bdf-af3e370899d2',
            'start' => '2019-01-01T00:00:00.000000Z',
            'end' => '2019-01-01T00:01:00.000000Z',
            'duration' => 60,
            'active' => true,
            'points' => 0
        ];

        Carbon::setTestNow('2019-01-01T00:00:00Z');

        Cache::shouldReceive('set')
            ->once()
            ->with(
                'game_e82c5370-840e-4906-8bdf-af3e370899d2',
                json_encode($gameData)
            );


        $game = $this->getTestGameModel();
        $game->start();

        $this->assertEquals($game->data, $gameData);
    }

    public function test_load()
    {
        $game = $this->getTestGameModel();
        $game->data = [
            'uuid' => 'e82c5370-840e-4906-8bdf-af3e370899d2',
            'start' => '2019-01-01T00:00:00.000000Z',
            'end' => '2019-01-01T00:01:00.000000Z',
            'active' => true,
            'points' => 0
        ];
        $this->invokeMethod($game, 'load');
        $this->assertNull($game->challenge);
    }

    public function test_load_challenge()
    {
        $game = $this->getTestGameModel();
        $game->data = [
            'uuid' => 'e82c5370-840e-4906-8bdf-af3e370899d2',
            'start' => '2019-01-01T00:00:00.000000Z',
            'end' => '2019-01-01T00:01:00.000000Z',
            'active' => true,
            'points' => 0,
            'challenge_key' => '4021dc15-eec7-4e10-b9f7-d9df1bfa96d9'
        ];

        $this->invokeMethod($game, 'load');
        $this->assertEquals($game->challenge->uuid, '4021dc15-eec7-4e10-b9f7-d9df1bfa96d9');
        $this->assertEquals($game->challenge->model_type, 'challenge');
        $this->assertNull($game->challenge->data);
    }

    public function test_create_challenge()
    {
        $mock = new MockHandler([
            new Response(200, [], json_encode([
                'results' => [
                    [
                        'id' => 12,
                        'original_title' => 'My awesome movie',
                        'poster_path' => '/test'
                    ]
                ]
            ])),
            new Response(200, [], json_encode([
                'cast' => [
                    [
                        'name' => 'Will smith',
                        'profile_path' => '/demo'
                    ]
                ]
            ])),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client = new Client(['handler' => $handlerStack]);

        Cache::shouldReceive('set')
            ->once()
            ->with(
                'challenge_8e8430e6-914e-462f-8954-5bcb839acb35',
                json_encode([
                    'uuid' => '8e8430e6-914e-462f-8954-5bcb839acb35',
                    'title' => 'My awesome movie',
                    'picture' => 'https://image.tmdb.org/t/p/w500/test',
                    'actor' => 'Will smith',
                    'actor_picture' => 'https://image.tmdb.org/t/p/w500/demo',
                    'is_valid' => true,
                    'game_uuid' => 'e82c5370-840e-4906-8bdf-af3e370899d2'
                ])
            );

        Cache::shouldReceive('set')
            ->once()
            ->with(
                'challenge_8e8430e6-914e-462f-8954-5bcb839acb35',
                json_encode([
                    'uuid' => '8e8430e6-914e-462f-8954-5bcb839acb35',
                    'title' => 'My awesome movie',
                    'picture' => 'https://image.tmdb.org/t/p/w500/test',
                    'actor' => 'Will smith',
                    'actor_picture' => 'https://image.tmdb.org/t/p/w500/demo',
                    'is_valid' => true
                ])
            );

        Cache::shouldReceive('set')
            ->once()
            ->with(
                'game_e82c5370-840e-4906-8bdf-af3e370899d2',
                json_encode([
                    'challenge_key' => '8e8430e6-914e-462f-8954-5bcb839acb35',
                ])
            );

        $game = $this->getTestGameModel();
        $game->client = $client;
        $game->data = [
            'challenge_key' => null,
        ];

        \Mockery::mock('alias:' . Uuid::class, [
            'uuid4' => '8e8430e6-914e-462f-8954-5bcb839acb35',
        ]);



        $challenge = $game->challenge();
        $this->assertEquals($challenge, [
            'uuid' => '8e8430e6-914e-462f-8954-5bcb839acb35',
            'title' => 'My awesome movie',
            'picture' => 'https://image.tmdb.org/t/p/w500/test',
            'actor' => 'Will smith',
            'actor_picture' => 'https://image.tmdb.org/t/p/w500/demo',
            'game_uuid' => 'e82c5370-840e-4906-8bdf-af3e370899d2'
        ]);
    }

    public function test_get_challenge()
    {

        Cache::shouldReceive('get')
            ->once()
            ->with(
                'challenge_5dd30ed1-c4f1-4095-bfe6-6271d5408695'
            )->andReturn(
                json_encode([
                    'uuid' => '5dd30ed1-c4f1-4095-bfe6-6271d5408695',
                    'title' => 'My awesome movie',
                    'picture' => 'https://mypicture.jpg',
                    'actor' => 'Will smith',
                    'is_valid' => true,
                    'game_uuid' => 'e82c5370-840e-4906-8bdf-af3e370899d2'
                ])
            );


        $game = $this->getTestGameModel();
        $game->data = [
            'challenge_key' => '5dd30ed1-c4f1-4095-bfe6-6271d5408695',
        ];

        $challenge = $game->challenge();
        $this->assertEquals($challenge, [
            'uuid' => '5dd30ed1-c4f1-4095-bfe6-6271d5408695',
            'title' => 'My awesome movie',
            'picture' => 'https://mypicture.jpg',
            'actor' => 'Will smith',
            'game_uuid' => 'e82c5370-840e-4906-8bdf-af3e370899d2'
        ]);
    }

    public function test_reset_challenge()
    {
        Cache::shouldReceive('set')
            ->once()
            ->with(
                'game_e82c5370-840e-4906-8bdf-af3e370899d2',
                json_encode([
                    'challenge_key' => null,
                ])
            );


        $game = $this->getTestGameModel();
        $game->data = [
            'challenge_key' => '4021dc15-eec7-4e10-b9f7-d9df1bfa96d9'
        ];

        $game->challenge = [
            'test' => 'demo'
        ];

        $game->resetChallenge();
        $this->assertNull($game->data['challenge_key']);
        $this->assertNull($game->challenge);
    }

    public function test_stop_game()
    {
        Cache::shouldReceive('set')
            ->once()
            ->with(
                'game_e82c5370-840e-4906-8bdf-af3e370899d2',
                json_encode([
                    'active' => false,
                ])
            );


        $game = $this->getTestGameModel();
        $game->data = [
            'active' => true
        ];

        $game->challenge = [
            'test' => 'demo'
        ];

        $this->invokeMethod($game, 'stopGame');
        $this->assertFalse($game->data['active']);
    }

    public function test_increment_points()
    {
        Cache::shouldReceive('set')
            ->once()
            ->with(
                'game_e82c5370-840e-4906-8bdf-af3e370899d2',
                json_encode([
                    'points' => 42,
                ])
            );


        $game = $this->getTestGameModel();
        $game->data = [
            'points' => 41
        ];

        $this->invokeMethod($game, 'incrementPoints');
        $this->assertEquals($game->data['points'], 42);
    }

    public function test_playable_before_time() {
        Carbon::setTestNow('2019-01-01T00:00:00Z');
        $game = $this->getTestGameModel();
        $game->data = [
            'start' => '2019-01-01T00:01:00Z',
            'end' => '2019-01-01T00:02:00Z'
        ];
        $playable = $this->invokeMethod($game, 'isPlayable');
        $this->assertFalse($playable);
    }

    public function test_playable_after_time() {
        Carbon::setTestNow('2019-01-01T00:03:00Z');
        $game = $this->getTestGameModel();
        $game->data = [
            'start' => '2019-01-01T00:01:00Z',
            'end' => '2019-01-01T00:02:00Z'
        ];
        $playable = $this->invokeMethod($game, 'isPlayable');
        $this->assertFalse($playable);
    }

    public function test_playable_during_time() {
        Carbon::setTestNow('2019-01-01T00:01:30Z');
        $game = $this->getTestGameModel();
        $game->data = [
            'start' => '2019-01-01T00:01:00Z',
            'end' => '2019-01-01T00:02:00Z',
        ];
        $playable = $this->invokeMethod($game, 'isPlayable');
        $this->assertTrue($playable);
    }

    public function test_playable_with_active_status() {
        Carbon::setTestNow('2019-01-01T00:01:30Z');
        $game = $this->getTestGameModel();
        $game->data = [
            'active' => true
        ];
        $playable = $this->invokeMethod($game, 'isPlayable');
        $this->assertTrue($playable);
    }

    public function test_playable_with_inactive_status() {
        Carbon::setTestNow('2019-01-01T00:01:30Z');
        $game = $this->getTestGameModel();
        $game->data = [
            'active' => false
        ];
        $playable = $this->invokeMethod($game, 'isPlayable');
        $this->assertFalse($playable);
    }
}
