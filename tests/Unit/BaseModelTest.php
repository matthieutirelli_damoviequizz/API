<?php

namespace Tests\Unit;

use Illuminate\Support\Facades\Cache;
use PHPUnit\Framework\TestCase;
use App\RedisModel\BaseModel;

class BaseModelTest extends TestCase
{
    private function getTestBaseModel() {
        return new BaseModel('57d6295d-df12-49d6-ae0d-bf275a9f1464');
    }

    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    public function test_constructor()
    {
        $baseModel = $this->getTestBaseModel();
        $this->assertEquals($baseModel->uuid, '57d6295d-df12-49d6-ae0d-bf275a9f1464');
        $this->assertNull($baseModel->model_type);
        $this->assertNull($baseModel->data);
    }

    public function test_constructor_without_uuid()
    {
        try {
            $baseModel = new BaseModel('');
        } catch (\Exception $exception) {
            $this->assertInstanceOf(\Exception::class, $exception);
            return;
        }

        $this->fail('No exception was thrown on constructor');
    }

    public function test_get_redis_key()
    {
        $baseModel = $this->getTestBaseModel();
        $baseModel->model_type = "testmodel";

        $this->assertEquals(
            $this->invokeMethod($baseModel, 'getRedisKey'),
            'testmodel_57d6295d-df12-49d6-ae0d-bf275a9f1464'
        );
    }

    public function test_get() {

        $baseModel = $this->getTestBaseModel();
        $baseModel->model_type = "testmodel";
        $baseModel->data = [
            'test_data' => 4242,
            'demo_data' => 1234
        ];

        $this->assertEquals($baseModel->get('test_data'), '4242');
    }

    public function test_set() {

        Cache::shouldReceive('set')
            ->once()
            ->with(
                'testmodel_57d6295d-df12-49d6-ae0d-bf275a9f1464',
                json_encode([
                    'test' => '4242'
                ])
            );

        $baseModel = $this->getTestBaseModel();
        $baseModel->model_type = "testmodel";
        $baseModel->data = [
            'test' => null,
        ];

        $baseModel->set('test', '4242');

        $this->assertEquals($baseModel->data['test'], '4242');
    }

    public function test_load() {

        Cache::shouldReceive('get')
            ->once()
            ->with(
                'testmodel_57d6295d-df12-49d6-ae0d-bf275a9f1464'
            )->andReturn(json_encode([
                'test' => '1234'
            ]));

        $baseModel = $this->getTestBaseModel();
        $baseModel->model_type = "testmodel";
        $this->invokeMethod($baseModel, 'load');

        $this->assertEquals($baseModel->data['test'], '1234');
    }

    public function test_get_data() {
        $baseModel = $this->getTestBaseModel();
        $baseModel->model_type = "testmodel";
        $baseModel->data = [
            'test' => 'bonjour',
        ];

        $this->assertEquals($baseModel->getData(), ['test' => 'bonjour']);
    }

    public function test_save() {

        Cache::shouldReceive('set')
            ->once()
            ->with(
                'testmodel_57d6295d-df12-49d6-ae0d-bf275a9f1464',
                json_encode([
                    'test' => '4250'
                ])
            );

        $baseModel = $this->getTestBaseModel();
        $baseModel->model_type = "testmodel";
        $baseModel->data = [
            'test' => '4250',
        ];
        $baseModel->save();
        $this->assertEquals($baseModel->data['test'], '4250');
    }
}
